package main

import (
	"context"
	"fmt"
	"io"
	"log"

	"google.golang.org/grpc"

	"gitlab.com/jc88/proto-example/example.com/postpb"
)

func main() {

	fmt.Println("Hello, from Client!")
	cc, err := grpc.Dial("localhost:50051", grpc.WithInsecure())
	if err != nil {
		log.Fatalf("Could ot connect: %v", err)
	}
	defer cc.Close()

	c := postpb.NewPostServiceClient(cc)
	fmt.Printf("Created the Client %v\n", c)

	createPost(c)
	ListPost(c)
}

func createPost(c postpb.PostServiceClient) {
	fmt.Println("Creating a Post.")
	request := &postpb.CreatePostRequest{
		Post: &postpb.Post{
			AuthorId: "Joey",
			Title:    "My Very First Post",
			Content:  "Content of my post.",
		},
	}
	res, err := c.CreatePost(context.Background(), request)
	if err != nil {
		log.Fatalf("Unexpected error while creating a post: %v\n", err)
	}
	fmt.Printf("Post created Successfully: %v\n", res)
	postId := res.GetPost().GetId()

	//read Post
	fmt.Println("Reading the post")
	_, err2 := c.ReadPost(context.Background(), &postpb.ReadPostRequest{PostId: "incorrect-id"})
	if err2 != nil {
		fmt.Printf("Got error while reading a post: %v\n", err2)
	}

	readPostRes, err3 := c.ReadPost(context.Background(), &postpb.ReadPostRequest{PostId: postId})
	if err3 != nil {
		fmt.Printf("Got error while reading a post: %v\n", err3)
	}
	fmt.Printf("Read successful... Content of Post: %v\n", readPostRes)

	//update a post
	newPost := &postpb.Post{
		Id:       postId,
		AuthorId: "Changed Author",
		Title:    "My First Post Title (Updated)",
		Content:  "Content of my first Post (Updated). some additional contents!",
	}

	updateRes, updateErr := c.UpdatePost(context.Background(), &postpb.UpdatePostRequest{Post: newPost})
	if updateErr != nil {
		fmt.Printf("Error while updating the post:%v\n", updateErr)
	}

	fmt.Printf("Post is updated:%v\n", updateRes)

	//delete a post
	deleteRes, deleteErr := c.DeletePost(context.Background(), &postpb.DeletePostRequest{PostId: postId})
	if deleteErr != nil {
		fmt.Printf("Error while deleting the post:%v\n", deleteErr)
	}
	fmt.Printf("Post was deleted: %v\n", deleteRes)
}

func ListPost(c postpb.PostServiceClient) {
	fmt.Println("Fetching a list of Posts.")
	stream, err := c.ListPost(context.Background(), &postpb.ListPostRequest{})
	if err != nil {
		log.Fatalf("Error while calling ListPost() RPC: %v\n", err)
	}

	for {
		res, err := stream.Recv()
		if err == io.EOF {
			break
		}
		if err != nil {
			log.Fatalf("Something happened Wrong: %v\n", err)
		}
		fmt.Println(res.GetPost())
	}
}
