package main

import (
	"context"
	"fmt"
	"log"
	"net"
	"os"
	"os/signal"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"gitlab.com/jc88/proto-example/example.com/postpb"
)

var collection *mongo.Collection

// server is used to implement postpb.PostServiceServer
type server struct {
	// Embeded the unimplemented server
	postpb.UnimplementedPostServiceServer
}

type PostItem struct {
	ID       primitive.ObjectID `bson:"_id,omitempty"`
	AuthorID string             `bson:"author_id"`
	Title    string             `bson:"title"`
	Content  string             `bson:"content"`
}

func main() {
	//if we crash the go code, we get the go file name along with line number.
	log.SetFlags(log.LstdFlags | log.Lshortfile)

	fmt.Println("Hello, from Server!")

	fmt.Println("connecting to MongoDB!")
	mongoClient, ctx, cancel, err := connect("mongodb://localhost:27017")
	if err != nil {
		panic(err)
	}

	defer close(mongoClient, ctx, cancel)

	// Ping mongoDB with Ping method
	ping(mongoClient, ctx)

	collection = mongoClient.Database("grpc-demo").Collection("posts")

	lis, err := net.Listen("tcp", "0.0.0.0:50051")
	if err != nil {
		log.Fatalf("Failed to listen: %v", err)
	}

	grpcServer := grpc.NewServer()
	postpb.RegisterPostServiceServer(grpcServer, &server{})

	go func() {
		fmt.Println("Starting the Server...")
		if err := grpcServer.Serve(lis); err != nil {
			log.Fatalf("Failed to Serve: %v", err)
		}
	}()

	// wait for Control C to exit
	ch := make(chan os.Signal, 1)
	signal.Notify(ch, os.Interrupt)

	// Block until a signal is received.
	<-ch
	fmt.Println("Stopping the server.")
	grpcServer.Stop()
	fmt.Println("Closing the Listener")
	lis.Close()

}

func connect(uri string) (*mongo.Client, context.Context, context.CancelFunc, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)

	client, err := mongo.Connect(ctx, options.Client().ApplyURI(uri))
	return client, ctx, cancel, err
}

func ping(client *mongo.Client, ctx context.Context) error {
	if err := client.Ping(ctx, readpref.Primary()); err != nil {
		return err
	}
	fmt.Println("connected successfully to MongoDB database!")
	return nil
}

func close(client *mongo.Client, ctx context.Context, cancel context.CancelFunc) {
	fmt.Println("Closing MongoDB Connection!")
	defer cancel()
	defer func() {
		if err := client.Disconnect(ctx); err != nil {
			panic(err)
		}
	}()
}

func (server *server) CreatePost(ctx context.Context, request *postpb.CreatePostRequest) (*postpb.CreatePostResponse, error) {
	fmt.Printf("Received a request to create a post: %v\n", request)
	post := request.GetPost()

	postData := PostItem{
		AuthorID: post.GetAuthorId(),
		Title:    post.GetTitle(),
		Content:  post.GetContent(),
	}
	res, err := collection.InsertOne(context.Background(), &postData)
	if err != nil {
		return nil, status.Errorf(
			codes.Internal,
			fmt.Sprintf("Internal server error: %v", err),
		)
	}
	oid, ok := res.InsertedID.(primitive.ObjectID)
	if !ok {
		return nil, status.Errorf(
			codes.Internal,
			fmt.Sprintf("can not convert to oid: %v", err),
		)
	}

	return &postpb.CreatePostResponse{
		Post: &postpb.Post{
			Id:       oid.Hex(),
			AuthorId: post.GetAuthorId(),
			Title:    post.GetTitle(),
			Content:  post.GetContent(),
		},
	}, nil
}

func (*server) ReadPost(ctx context.Context, request *postpb.ReadPostRequest) (*postpb.ReadPostResponse, error) {
	fmt.Printf("Received a request to get a post: %v\n", request)
	postId := request.GetPostId()

	oId, err := primitive.ObjectIDFromHex(postId)
	if err != nil {
		return nil, status.Errorf(codes.InvalidArgument, fmt.Sprintf("Can not parse ID: %v", postId))
	}
	// create an empty struct
	postData := &PostItem{}
	filter := bson.D{{"_id", oId}}
	result := collection.FindOne(context.Background(), filter)

	if err := result.Decode(postData); err != nil {
		return nil, status.Errorf(codes.NotFound, fmt.Sprintf("Can not find post for specified ID: %v", err))
	}

	return &postpb.ReadPostResponse{
		Post: &postpb.Post{
			Id:       postData.ID.String(),
			AuthorId: postData.AuthorID,
			Title:    postData.Title,
			Content:  postData.Content,
		},
	}, nil
}

func (*server) UpdatePost(ctx context.Context, request *postpb.UpdatePostRequest) (*postpb.UpdatePostResponse, error) {
	fmt.Printf("Received a request to update a post: %v\n", request)
	post := request.GetPost()
	oId, err := primitive.ObjectIDFromHex(post.GetId())
	if err != nil {
		return nil, status.Errorf(codes.InvalidArgument, fmt.Sprintf("Can not parse post ID: %v\n", post.GetId()))
	}

	//crete an empty struct
	postData := &PostItem{}
	filter := bson.D{{"_id", oId}}
	result := collection.FindOne(context.Background(), filter)

	if err := result.Decode(postData); err != nil {
		return nil, status.Errorf(codes.NotFound, fmt.Sprintf("Can not find post for specified ID: %v\n", err))
	}
	// update our internal struct.
	postData.AuthorID = post.AuthorId
	postData.Title = post.Title
	postData.Content = post.Content

	res, err := collection.ReplaceOne(context.Background(), filter, postData)
	if err != nil {
		return nil, status.Errorf(codes.Internal, fmt.Sprintf("Internal Error While Updating the Post: %v\n", err))
	}
	fmt.Printf("Post updated successfully: %v\n", res)

	return &postpb.UpdatePostResponse{
		Post: postDataToPostpb(*postData),
	}, nil
}

func postDataToPostpb(data PostItem) *postpb.Post {
	return &postpb.Post{
		Id:       data.ID.Hex(),
		AuthorId: data.AuthorID,
		Title:    data.Title,
		Content:  data.Content,
	}
}

func (*server) DeletePost(ctx context.Context, request *postpb.DeletePostRequest) (*postpb.DeletePostResponse, error) {
	fmt.Printf("Received a request to delete a post: %v\n", request)
	postId := request.GetPostId()
	oId, err := primitive.ObjectIDFromHex(postId)
	if err != nil {
		return nil, status.Errorf(codes.InvalidArgument, fmt.Sprintf("Can not parse post ID: %v\n", postId))
	}
	filter := bson.D{{"_id", oId}}
	deleteRes, deleteErr := collection.DeleteOne(context.Background(), filter)
	if deleteErr != nil {
		return nil, status.Errorf(codes.Internal, fmt.Sprintf("Internal Error While Deleting the Post: %v\n", deleteErr))
	}
	if deleteRes.DeletedCount == 0 {
		return nil, status.Errorf(codes.NotFound, fmt.Sprintf("Post with ID %v not found in the DB to delete.\n", postId))
	}

	return &postpb.DeletePostResponse{PostId: request.GetPostId()}, nil
}

func (*server) ListPost(request *postpb.ListPostRequest, stream postpb.PostService_ListPostServer) error {
	fmt.Println("Received a request to return a list of post")
	cursor, err := collection.Find(context.Background(), bson.D{})
	if err != nil {
		return status.Errorf(codes.Internal, fmt.Sprintf("Internal Error: %v\n", err))
	}
	defer cursor.Close(context.Background())

	for cursor.Next(context.Background()) {
		// create an empty struct
		postData := &PostItem{}
		err := cursor.Decode(postData)
		if err != nil {
			status.Errorf(codes.Internal, fmt.Sprintf("Internal Error while decoding data from MongoDB:%v\n", err))
		}
		stream.Send(&postpb.ListPostResponse{Post: postDataToPostpb(*postData)})
	}
	if err := cursor.Err(); err != nil {
		return status.Errorf(codes.Internal, fmt.Sprintf("Unknown Internal Error: %v\n", err))
	}

	return nil
}
