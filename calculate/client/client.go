package main

import (
	"context"
	"fmt"
	"io"
	"log"
	"time"

	"google.golang.org/grpc"

	"gitlab.com/jc88/proto-example/example.com/hellopb"
)

func main() {

	fmt.Println("Hello, from Client!")
	cc, err := grpc.Dial("localhost:50051", grpc.WithInsecure())
	if err != nil {
		log.Fatalf("Could not connect: %v", err)
	}
	defer cc.Close()

	c2 := hellopb.NewCalculatorServiceClient(cc)

	// callUnaryRPCCalculate(c2)
	// callServerStreamingArithmeticOperations(c2)

	// callClientSideStreamingRPC(c2)

	callBiDirectionalStreamingRPC(c2)
}

func callUnaryRPCCalculate(c hellopb.CalculatorServiceClient) {
	calculateRequest := &hellopb.CalculateRequest{
		Numbers: &hellopb.Numbers{
			First:  8,
			Second: 10,
			Third:  6,
		},
	}

	// c.SayHello()

	response, err := c.Calculate(context.Background(), calculateRequest)
	if err != nil {
		log.Fatalf("Error while calling SayHello RPC: %v", err)
	}
	log.Printf("Response from SayHello: %v", response)
}

func callServerStreamingArithmeticOperations(c hellopb.CalculatorServiceClient) {
	fmt.Println("Calling Unary PerformArithmeticOperations RPC call")
	request := &hellopb.PerformArithmeticOperationsRequest{
		Numbers: &hellopb.TwoNumbers{
			First:  10,
			Second: 5,
		},
	}
	resStream, err := c.PerformArithmeticOperations(context.Background(), request)
	if err != nil {
		log.Fatalf("error while calling PerformArithmeticOperations() RPC: %v", err)
	}
	for {
		message, err := resStream.Recv()
		if err == io.EOF {
			// we reached the end of stream.
			break
		}
		if err != nil {
			log.Fatalf("error while reading the stream: %v", err)
		}
		log.Printf("Response from the PerformArithmeticOperations: %v", message.GetMessage())
	}

}

func callClientSideStreamingRPC(c hellopb.CalculatorServiceClient) {
	fmt.Println("Calling Client side streaming CalculateClientLongAverage() RPC call")
	requests := []*hellopb.LongCalculateAverageRequest{
		{
			First: 5,
		},
		{
			First: 6,
		},
		{
			First: 7,
		},
		{
			First: 8,
		},
	}
	stream, err := c.CalculateClientLongAverage(context.Background())
	if err != nil {
		log.Fatalf("Error while calling client side streaming RPC: %v", err)
	}
	// iterate the slice and send the data in the request.
	for _, req := range requests {
		fmt.Printf("Sending request for: %v\n", req)
		stream.Send(req)
		time.Sleep(2 * time.Second)
	}

	res, err := stream.CloseAndRecv()
	if err != nil {
		log.Fatalf("Error While reading the response: %v\n", err)
	}
	fmt.Printf("Response from LongHello Response: %v\n", res)
}

func callBiDirectionalStreamingRPC(c hellopb.CalculatorServiceClient) {
	fmt.Println("Calling Bi Directional streaming SayHelloEveryone() RPC call")
	requests := []*hellopb.FindManimumRequest{
		{
			First: 5,
		},
		{
			First: 3,
		},
		{
			First: 3,
		},
		{
			First: 6,
		},
		{
			First: 22,
		},
		{
			First: 20,
		},
		{First: 35},
		{First: 15},
	}

	stream, err := c.FindManimum(context.Background())
	if err != nil {
		log.Fatalf("Error while calling the Bi Directional streaming RPC API")
	}

	waitChan := make(chan struct{})
	// iterate over requests and send bunch of messages to the server (go-routine)
	go func() {
		// function to send messages to server
		for _, req := range requests {
			fmt.Printf("Sending a message: %v\n", req)
			stream.Send(req)
			time.Sleep(2 * time.Second)
		}
		stream.CloseSend()
	}()
	//receive bunch of messages from the server (go-routine)
	go func() {
		// function to receive messages from server
		for {
			res, err := stream.Recv()
			if err == io.EOF {
				break
			}
			if err != nil {
				log.Fatalf("Error while receiving: %v", err)
				break
			}
			fmt.Println("Received Response:", res.GetFirst())
		}
		close(waitChan)
	}()

	//block the main go-routines to finish above 2 go-routines
	<-waitChan
}
