package main

import (
	"context"
	"fmt"
	"io"
	"log"
	"net"
	"time"

	"google.golang.org/grpc"

	"gitlab.com/jc88/proto-example/example.com/hellopb"
)

func main() {
	fmt.Println("Hello, from Server!")

	lis, err := net.Listen("tcp", "0.0.0.0:50051")
	if err != nil {
		log.Fatalf("Failed to listen: %v", err)
	}

	grpcServer := grpc.NewServer()

	hellopb.RegisterCalculatorServiceServer(grpcServer, &server2{})

	if err := grpcServer.Serve(lis); err != nil {
		log.Fatalf("Failed to Serve: %v", err)
	}
}

// server is used to implement hellopb.HelloServiceServer
type server2 struct {
	// Embeded the unimplemented server
	hellopb.UnimplementedCalculatorServiceServer
}

func (*server2) Calculate(ctx context.Context, request *hellopb.CalculateRequest) (*hellopb.CalculateResponse, error) {
	fmt.Printf("SayHello() invoked with %v", request)
	first := request.GetNumbers().GetFirst()
	last := request.GetNumbers().GetSecond()
	third := request.GetNumbers().GetThird()

	av := float32((first + last + third)) / 3

	response := &hellopb.CalculateResponse{
		Average: av,
	}

	return response, nil
}

// want PerformArithmeticOperations(*hellopb.PerformArithmeticOperationsRequest, hellopb.CalculatorService_PerformArithmeticOperationsServer) error
func (*server2) PerformArithmeticOperations(request *hellopb.PerformArithmeticOperationsRequest, stream hellopb.CalculatorService_PerformArithmeticOperationsServer) error {
	fmt.Printf("ArithmeticOperations() invoked with %v", request)
	first := request.GetNumbers().GetFirst()
	last := request.GetNumbers().GetSecond()

	fs := make([]func(x, y int32) string, 0)

	fs = append(fs, Addition)
	fs = append(fs, Subtraction)
	fs = append(fs, Multiplication)
	fs = append(fs, Division)

	for _, f := range fs {
		q1 := f(first, last)
		response := &hellopb.PerformArithmeticOperationsResponse{
			Message: q1,
		}
		fmt.Printf("response: %s", q1)
		stream.Send(response)
		time.Sleep(2 * time.Second)
	}
	return nil
}

// func (*server2) SayHelloManyTimes(request *hellopb.HelloManyTimesRequest, stream hellopb.HelloService_SayHelloManyTimesServer) error {
// 	fmt.Printf("SayHelloManyTimes() invoked with %v", request)
// 	firstName := request.GetGreet().GetFirstName()
// 	lastName := request.GetGreet().GetLastName()
// 	for i := 0; i < 10; i++ {
// 		response := &hellopb.HelloManyTimesResponse{
// 			Message: "Hello, " + firstName + " " + lastName + "!" + " Number: " + strconv.Itoa(i),
// 		}
// 		stream.Send(response)
// 		time.Sleep(2 * time.Second)
// 	}
// 	return nil
// }

func Addition(x, y int32) string {
	z := x + y
	fmt.Printf("result: %d", z)
	return string(z)
}

func Subtraction(x, y int32) string {
	z := x - y
	fmt.Printf("result: %d", z)
	return string(z)
}

func Multiplication(x, y int32) string {
	z := x * y
	fmt.Printf("result: %d", z)
	return string(z)
}

func Division(x, y int32) string {
	z := x / y
	fmt.Printf("result: %d", z)
	return string(z)
}

func (*server2) CalculateClientLongAverage(stream hellopb.CalculatorService_CalculateClientLongAverageServer) error {
	fmt.Printf("CalculateClientLongAverage() invoked with streaming client request\n")
	var sum int32
	var count int32
	for {
		count++

		fmt.Printf("Sum: %d, count: %d", sum, count)

		req, err := stream.Recv()
		if err == io.EOF {
			res := float64(sum) / float64(count)

			q1 := fmt.Sprintf("%.2f", res)

			fmt.Printf("result: %.2f", res)
			response := &hellopb.LongCalculateAverageResponse{
				Message: q1,
			}
			return stream.SendAndClose(response)
		}
		if err != nil {
			log.Fatalf("Error while reading the client stream: %v", err)
		}

		_ = req
		sum += req.GetFirst()
	}
}

func (*server2) FindManimum(stream hellopb.CalculatorService_FindManimumServer) error {
	fmt.Printf("FindManimum() invoked with Bi Di streaming client request\n")

	maxVal := 0

	for {
		req, err := stream.Recv()
		if err == io.EOF {
			return nil
		}
		if err != nil {
			log.Fatalf("Error while reading client stream: %v", err)
			return err
		}

		current := req.GetFirst()
		if current > int32(maxVal) {
			maxVal = int(current)
		}

		res := &hellopb.FindManimumResponse{
			First: int32(maxVal),
		}

		sendErr := stream.Send(res)
		if sendErr != nil {
			log.Fatalf("Error while sending data to client: %v", sendErr)
		}
	}
}
