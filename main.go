package main

import (
	"fmt"
	"log"
	"net"

	"google.golang.org/grpc"

	"gitlab.com/jc88/proto-example/example.com/hellopb"
	"gitlab.com/jc88/proto-example/go/example.com/enumpb"
	"gitlab.com/jc88/proto-example/go/example.com/personpb"
)

func main() {
	usePerson()
}

func usePerson() {

	joey := personpb.Person{
		FistName:   "Joey",
		LastName:   "Tribiyani",
		Age:        30,
		IsVerified: true,
	}
	// print Person struct
	fmt.Println(joey.Age)
	// modify the age of person
	joey.Age = 35
	// print Person struct
	fmt.Println(joey.Age)

	//get First Name using getter method.
	fmt.Println("Persons First Name:", joey.GetFistName())

	enumExample()

	fmt.Println("Hello, from Server!")

	lis, err := net.Listen("tcp", "0.0.0.0:50051")
	if err != nil {
		log.Fatalf("Failed to listen: %v", err)
	}

	grpcServer := grpc.NewServer()
	hellopb.RegisterHelloServiceServer(grpcServer, &server{})

	if err := grpcServer.Serve(lis); err != nil {
		log.Fatalf("Failed to Serve: %v", err)
	}
}

func enumExample() {
	em := enumpb.EnumMessage{
		Id:           15,
		DayOfTheWeek: enumpb.DayOfTheWeek_FRIDAY,
	}
	fmt.Println(em.Id)
}

// server is used to implement hellopb.HelloServiceServer
type server struct {
	// Embeded the unimplemented server
	hellopb.UnimplementedHelloServiceServer
}
