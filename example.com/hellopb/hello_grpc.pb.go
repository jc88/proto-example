// Code generated by protoc-gen-go-grpc. DO NOT EDIT.
// versions:
// - protoc-gen-go-grpc v1.2.0
// - protoc             v3.19.4
// source: hello.proto

package hellopb

import (
	context "context"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
// Requires gRPC-Go v1.32.0 or later.
const _ = grpc.SupportPackageIsVersion7

// HelloServiceClient is the client API for HelloService service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type HelloServiceClient interface {
	// Unary API
	SayHello(ctx context.Context, in *HelloRequest, opts ...grpc.CallOption) (*HelloResponse, error)
	// Server streaming API
	SayHelloManyTimes(ctx context.Context, in *HelloManyTimesRequest, opts ...grpc.CallOption) (HelloService_SayHelloManyTimesClient, error)
	// client side streaming
	SayLongHello(ctx context.Context, opts ...grpc.CallOption) (HelloService_SayLongHelloClient, error)
	// Bi directional streaming
	SayHelloEveryone(ctx context.Context, opts ...grpc.CallOption) (HelloService_SayHelloEveryoneClient, error)
	// Unary with deadline
	SayHelloWithDeadline(ctx context.Context, in *HelloWithDeadlineRequest, opts ...grpc.CallOption) (*HelloWithDeadlineResponse, error)
}

type helloServiceClient struct {
	cc grpc.ClientConnInterface
}

func NewHelloServiceClient(cc grpc.ClientConnInterface) HelloServiceClient {
	return &helloServiceClient{cc}
}

func (c *helloServiceClient) SayHello(ctx context.Context, in *HelloRequest, opts ...grpc.CallOption) (*HelloResponse, error) {
	out := new(HelloResponse)
	err := c.cc.Invoke(ctx, "/hello.HelloService/SayHello", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *helloServiceClient) SayHelloManyTimes(ctx context.Context, in *HelloManyTimesRequest, opts ...grpc.CallOption) (HelloService_SayHelloManyTimesClient, error) {
	stream, err := c.cc.NewStream(ctx, &HelloService_ServiceDesc.Streams[0], "/hello.HelloService/SayHelloManyTimes", opts...)
	if err != nil {
		return nil, err
	}
	x := &helloServiceSayHelloManyTimesClient{stream}
	if err := x.ClientStream.SendMsg(in); err != nil {
		return nil, err
	}
	if err := x.ClientStream.CloseSend(); err != nil {
		return nil, err
	}
	return x, nil
}

type HelloService_SayHelloManyTimesClient interface {
	Recv() (*HelloManyTimesResponse, error)
	grpc.ClientStream
}

type helloServiceSayHelloManyTimesClient struct {
	grpc.ClientStream
}

func (x *helloServiceSayHelloManyTimesClient) Recv() (*HelloManyTimesResponse, error) {
	m := new(HelloManyTimesResponse)
	if err := x.ClientStream.RecvMsg(m); err != nil {
		return nil, err
	}
	return m, nil
}

func (c *helloServiceClient) SayLongHello(ctx context.Context, opts ...grpc.CallOption) (HelloService_SayLongHelloClient, error) {
	stream, err := c.cc.NewStream(ctx, &HelloService_ServiceDesc.Streams[1], "/hello.HelloService/sayLongHello", opts...)
	if err != nil {
		return nil, err
	}
	x := &helloServiceSayLongHelloClient{stream}
	return x, nil
}

type HelloService_SayLongHelloClient interface {
	Send(*LongHelloRequest) error
	CloseAndRecv() (*LongHelloResponse, error)
	grpc.ClientStream
}

type helloServiceSayLongHelloClient struct {
	grpc.ClientStream
}

func (x *helloServiceSayLongHelloClient) Send(m *LongHelloRequest) error {
	return x.ClientStream.SendMsg(m)
}

func (x *helloServiceSayLongHelloClient) CloseAndRecv() (*LongHelloResponse, error) {
	if err := x.ClientStream.CloseSend(); err != nil {
		return nil, err
	}
	m := new(LongHelloResponse)
	if err := x.ClientStream.RecvMsg(m); err != nil {
		return nil, err
	}
	return m, nil
}

func (c *helloServiceClient) SayHelloEveryone(ctx context.Context, opts ...grpc.CallOption) (HelloService_SayHelloEveryoneClient, error) {
	stream, err := c.cc.NewStream(ctx, &HelloService_ServiceDesc.Streams[2], "/hello.HelloService/SayHelloEveryone", opts...)
	if err != nil {
		return nil, err
	}
	x := &helloServiceSayHelloEveryoneClient{stream}
	return x, nil
}

type HelloService_SayHelloEveryoneClient interface {
	Send(*HelloEveryoneRequest) error
	Recv() (*HelloEveryoneResponse, error)
	grpc.ClientStream
}

type helloServiceSayHelloEveryoneClient struct {
	grpc.ClientStream
}

func (x *helloServiceSayHelloEveryoneClient) Send(m *HelloEveryoneRequest) error {
	return x.ClientStream.SendMsg(m)
}

func (x *helloServiceSayHelloEveryoneClient) Recv() (*HelloEveryoneResponse, error) {
	m := new(HelloEveryoneResponse)
	if err := x.ClientStream.RecvMsg(m); err != nil {
		return nil, err
	}
	return m, nil
}

func (c *helloServiceClient) SayHelloWithDeadline(ctx context.Context, in *HelloWithDeadlineRequest, opts ...grpc.CallOption) (*HelloWithDeadlineResponse, error) {
	out := new(HelloWithDeadlineResponse)
	err := c.cc.Invoke(ctx, "/hello.HelloService/SayHelloWithDeadline", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// HelloServiceServer is the server API for HelloService service.
// All implementations must embed UnimplementedHelloServiceServer
// for forward compatibility
type HelloServiceServer interface {
	// Unary API
	SayHello(context.Context, *HelloRequest) (*HelloResponse, error)
	// Server streaming API
	SayHelloManyTimes(*HelloManyTimesRequest, HelloService_SayHelloManyTimesServer) error
	// client side streaming
	SayLongHello(HelloService_SayLongHelloServer) error
	// Bi directional streaming
	SayHelloEveryone(HelloService_SayHelloEveryoneServer) error
	// Unary with deadline
	SayHelloWithDeadline(context.Context, *HelloWithDeadlineRequest) (*HelloWithDeadlineResponse, error)
	mustEmbedUnimplementedHelloServiceServer()
}

// UnimplementedHelloServiceServer must be embedded to have forward compatible implementations.
type UnimplementedHelloServiceServer struct {
}

func (UnimplementedHelloServiceServer) SayHello(context.Context, *HelloRequest) (*HelloResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method SayHello not implemented")
}
func (UnimplementedHelloServiceServer) SayHelloManyTimes(*HelloManyTimesRequest, HelloService_SayHelloManyTimesServer) error {
	return status.Errorf(codes.Unimplemented, "method SayHelloManyTimes not implemented")
}
func (UnimplementedHelloServiceServer) SayLongHello(HelloService_SayLongHelloServer) error {
	return status.Errorf(codes.Unimplemented, "method SayLongHello not implemented")
}
func (UnimplementedHelloServiceServer) SayHelloEveryone(HelloService_SayHelloEveryoneServer) error {
	return status.Errorf(codes.Unimplemented, "method SayHelloEveryone not implemented")
}
func (UnimplementedHelloServiceServer) SayHelloWithDeadline(context.Context, *HelloWithDeadlineRequest) (*HelloWithDeadlineResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method SayHelloWithDeadline not implemented")
}
func (UnimplementedHelloServiceServer) mustEmbedUnimplementedHelloServiceServer() {}

// UnsafeHelloServiceServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to HelloServiceServer will
// result in compilation errors.
type UnsafeHelloServiceServer interface {
	mustEmbedUnimplementedHelloServiceServer()
}

func RegisterHelloServiceServer(s grpc.ServiceRegistrar, srv HelloServiceServer) {
	s.RegisterService(&HelloService_ServiceDesc, srv)
}

func _HelloService_SayHello_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(HelloRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(HelloServiceServer).SayHello(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/hello.HelloService/SayHello",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(HelloServiceServer).SayHello(ctx, req.(*HelloRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _HelloService_SayHelloManyTimes_Handler(srv interface{}, stream grpc.ServerStream) error {
	m := new(HelloManyTimesRequest)
	if err := stream.RecvMsg(m); err != nil {
		return err
	}
	return srv.(HelloServiceServer).SayHelloManyTimes(m, &helloServiceSayHelloManyTimesServer{stream})
}

type HelloService_SayHelloManyTimesServer interface {
	Send(*HelloManyTimesResponse) error
	grpc.ServerStream
}

type helloServiceSayHelloManyTimesServer struct {
	grpc.ServerStream
}

func (x *helloServiceSayHelloManyTimesServer) Send(m *HelloManyTimesResponse) error {
	return x.ServerStream.SendMsg(m)
}

func _HelloService_SayLongHello_Handler(srv interface{}, stream grpc.ServerStream) error {
	return srv.(HelloServiceServer).SayLongHello(&helloServiceSayLongHelloServer{stream})
}

type HelloService_SayLongHelloServer interface {
	SendAndClose(*LongHelloResponse) error
	Recv() (*LongHelloRequest, error)
	grpc.ServerStream
}

type helloServiceSayLongHelloServer struct {
	grpc.ServerStream
}

func (x *helloServiceSayLongHelloServer) SendAndClose(m *LongHelloResponse) error {
	return x.ServerStream.SendMsg(m)
}

func (x *helloServiceSayLongHelloServer) Recv() (*LongHelloRequest, error) {
	m := new(LongHelloRequest)
	if err := x.ServerStream.RecvMsg(m); err != nil {
		return nil, err
	}
	return m, nil
}

func _HelloService_SayHelloEveryone_Handler(srv interface{}, stream grpc.ServerStream) error {
	return srv.(HelloServiceServer).SayHelloEveryone(&helloServiceSayHelloEveryoneServer{stream})
}

type HelloService_SayHelloEveryoneServer interface {
	Send(*HelloEveryoneResponse) error
	Recv() (*HelloEveryoneRequest, error)
	grpc.ServerStream
}

type helloServiceSayHelloEveryoneServer struct {
	grpc.ServerStream
}

func (x *helloServiceSayHelloEveryoneServer) Send(m *HelloEveryoneResponse) error {
	return x.ServerStream.SendMsg(m)
}

func (x *helloServiceSayHelloEveryoneServer) Recv() (*HelloEveryoneRequest, error) {
	m := new(HelloEveryoneRequest)
	if err := x.ServerStream.RecvMsg(m); err != nil {
		return nil, err
	}
	return m, nil
}

func _HelloService_SayHelloWithDeadline_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(HelloWithDeadlineRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(HelloServiceServer).SayHelloWithDeadline(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/hello.HelloService/SayHelloWithDeadline",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(HelloServiceServer).SayHelloWithDeadline(ctx, req.(*HelloWithDeadlineRequest))
	}
	return interceptor(ctx, in, info, handler)
}

// HelloService_ServiceDesc is the grpc.ServiceDesc for HelloService service.
// It's only intended for direct use with grpc.RegisterService,
// and not to be introspected or modified (even as a copy)
var HelloService_ServiceDesc = grpc.ServiceDesc{
	ServiceName: "hello.HelloService",
	HandlerType: (*HelloServiceServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "SayHello",
			Handler:    _HelloService_SayHello_Handler,
		},
		{
			MethodName: "SayHelloWithDeadline",
			Handler:    _HelloService_SayHelloWithDeadline_Handler,
		},
	},
	Streams: []grpc.StreamDesc{
		{
			StreamName:    "SayHelloManyTimes",
			Handler:       _HelloService_SayHelloManyTimes_Handler,
			ServerStreams: true,
		},
		{
			StreamName:    "sayLongHello",
			Handler:       _HelloService_SayLongHello_Handler,
			ClientStreams: true,
		},
		{
			StreamName:    "SayHelloEveryone",
			Handler:       _HelloService_SayHelloEveryone_Handler,
			ServerStreams: true,
			ClientStreams: true,
		},
	},
	Metadata: "hello.proto",
}
