package main

import (
	"context"
	"fmt"
	"io"
	"log"
	"net"
	"time"

	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"gitlab.com/jc88/proto-example/example.com/hellopb"
)

func main() {
	fmt.Println("Hello, from Server!")

	lis, err := net.Listen("tcp", "0.0.0.0:50051")
	if err != nil {
		log.Fatalf("Failed to listen: %v", err)
	}

	grpcServer := grpc.NewServer()
	hellopb.RegisterHelloServiceServer(grpcServer, &server{})

	hellopb.RegisterCalculatorServiceServer(grpcServer, &server2{})

	if err := grpcServer.Serve(lis); err != nil {
		log.Fatalf("Failed to Serve: %v", err)
	}
}

// server is used to implement hellopb.HelloServiceServer
type server struct {
	// Embeded the unimplemented server
	hellopb.UnimplementedHelloServiceServer

	// hellopb.UnimplementedCalculatorServiceServer
}

func (*server) SayHello(ctx context.Context, request *hellopb.HelloRequest) (*hellopb.HelloResponse, error) {
	fmt.Printf("SayHello() invoked with %v", request)
	firstName := request.GetGreet().GetFirstName()
	lastName := request.GetGreet().GetLastName()
	response := &hellopb.HelloResponse{
		Message: "Hello, " + firstName + " " + lastName + "!",
	}

	return response, nil
}

func (*server) SayLongHello(stream hellopb.HelloService_SayLongHelloServer) error {
	fmt.Printf("SayLongHello() invoked with streaming client request\n")
	message := ""
	for {
		req, err := stream.Recv()
		if err == io.EOF {
			// we have finished reading the client stream
			response := &hellopb.LongHelloResponse{
				Message: message,
			}
			return stream.SendAndClose(response)
		}
		if err != nil {
			log.Fatalf("Error while reading the client stream: %v", err)
		}
		message += "Hello " + req.GetGreet().GetFirstName() + " " + req.GetGreet().GetLastName() + "!, "
	}
}

func (*server) SayHelloEveryone(stream hellopb.HelloService_SayHelloEveryoneServer) error {
	fmt.Printf("SayHelloEveryone() invoked with Bi Di streaming client request\n")

	for {
		req, err := stream.Recv()
		if err == io.EOF {
			return nil
		}
		if err != nil {
			log.Fatalf("Error while reading client stream: %v", err)
			return err
		}
		message := "Hello, " + req.GetGreet().GetFirstName() + " " + req.GetGreet().GetLastName() + "!"
		res := &hellopb.HelloEveryoneResponse{
			Message: message,
		}

		sendErr := stream.Send(res)
		if sendErr != nil {
			log.Fatalf("Error while sending data to client: %v", sendErr)
		}
	}
}

func (*server) SayHelloWithDeadline(ctx context.Context, request *hellopb.HelloWithDeadlineRequest) (*hellopb.HelloWithDeadlineResponse, error) {
	fmt.Printf("SayHelloWithDeadline() invoked with %v\n", request)
	for i := 0; i < 3; i++ {
		if ctx.Err() == context.Canceled {
			// the client cancelled the request
			fmt.Println("The client cancelled the request!")
			return nil, status.Errorf(codes.Canceled, "The client cancelled the request")
		}
		time.Sleep(1 * time.Second)
	}
	firstName := request.GetGreet().GetFirstName()
	lastName := request.GetGreet().GetLastName()
	response := &hellopb.HelloWithDeadlineResponse{
		Message: "Hello, " + firstName + " " + lastName + "!",
	}

	return response, nil
}

// server is used to implement hellopb.HelloServiceServer
type server2 struct {
	// Embeded the unimplemented server
	hellopb.UnimplementedCalculatorServiceServer
}

// func (*server) SayHelloManyTimes(request *hellopb.HelloManyTimesRequest, stream hellopb.HelloService_SayHelloManyTimesServer) error {
// 	fmt.Printf("SayHelloManyTimes() invoked with %v", request)
// 	firstName := request.GetGreet().GetFirstName()
// 	lastName := request.GetGreet().GetLastName()
// 	for i := 0; i < 10; i++ {
// 		response := &hellopb.HelloManyTimesResponse{
// 			Message: "Hello, " + firstName + " " + lastName + "!" + " Number: " + strconv.Itoa(i),
// 		}
// 		stream.Send(response)
// 		time.Sleep(2 * time.Second)
// 	}
// 	return nil
// }

func (*server2) Calculate(ctx context.Context, request *hellopb.CalculateRequest) (*hellopb.CalculateResponse, error) {
	fmt.Printf("SayHello() invoked with %v", request)
	first := request.GetNumbers().GetFirst()
	last := request.GetNumbers().GetSecond()
	third := request.GetNumbers().GetThird()

	av := float32((first + last + third)) / 3

	response := &hellopb.CalculateResponse{
		Average: av,
	}

	return response, nil
}

func (*server2) ArithmeticOperations(ctx context.Context, request *hellopb.PerformArithmeticOperationsRequest, stream hellopb.CalculatorService_PerformArithmeticOperationsServer) error {
	fmt.Printf("ArithmeticOperations() invoked with %v", request)
	first := request.GetNumbers().GetFirst()
	last := request.GetNumbers().GetSecond()

	// fs := make([]func(x, y int32) string, 0)

	// for _, f := range fs {
	// 	q1 := f(first, last)
	// 	response := &hellopb.PerformArithmeticOperationsResponse{
	// 		Message: q1,
	// 	}
	// 	stream.Send(response)
	// 	time.Sleep(2 * time.Second)
	// }

	res := Addition(first, last)
	response := &hellopb.PerformArithmeticOperationsResponse{
		Message: res,
	}
	stream.Send(response)

	res = Subtraction(first, last)
	response = &hellopb.PerformArithmeticOperationsResponse{
		Message: res,
	}
	stream.Send(response)

	return nil
}

func Addition(x, y int32) string {
	z := x + y
	return string(z)
}

func Subtraction(x, y int32) string {
	z := x - y
	return string(z)
}

func Multiplication(x, y int32) string {
	z := x * y
	return string(z)
}

func Division(x, y int32) string {
	z := x / y
	return string(z)
}
