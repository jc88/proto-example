package main

import (
	"context"
	"fmt"
	"io"
	"log"
	"time"

	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"gitlab.com/jc88/proto-example/example.com/hellopb"
)

func main() {

	fmt.Println("Hello, from Client!")
	cc, err := grpc.Dial("localhost:50051", grpc.WithInsecure())
	if err != nil {
		log.Fatalf("Could not connect: %v", err)
	}
	defer cc.Close()

	c := hellopb.NewHelloServiceClient(cc)

	c2 := hellopb.NewCalculatorServiceClient(cc)

	fmt.Printf("Created the Client %v", c)

	callUnaryRPC(c)
	// callServerStreaming(c)
	// callClientSideStreamingRPC(c)
	// callBiDirectionalStreamingRPC(c)
	callUnaryRPCWithDeadline(c, time.Duration(1*time.Second))

	callUnaryRPCCalculate(c2)
	// callServerStreamingArithmeticOperations(c2)
}

func callUnaryRPC(c hellopb.HelloServiceClient) {
	fmt.Println("Calling Unary SayHello RPC call")
	helloRequest := &hellopb.HelloRequest{
		Greet: &hellopb.Hello{
			FirstName: "Chandler",
			LastName:  "Bing",
		},
	}
	response, err := c.SayHello(context.Background(), helloRequest)
	if err != nil {
		log.Fatalf("Error while calling SayHello RPC: %v", err)
	}
	log.Printf("Response from SayHello: %v", response)
}

func callUnaryRPCCalculate(c hellopb.CalculatorServiceClient) {
	calculateRequest := &hellopb.CalculateRequest{
		Numbers: &hellopb.Numbers{
			First:  8,
			Second: 10,
			Third:  6,
		},
	}

	// c.SayHello()

	response, err := c.Calculate(context.Background(), calculateRequest)
	if err != nil {
		log.Fatalf("Error while calling SayHello RPC: %v", err)
	}
	log.Printf("Response from SayHello: %v", response)
}

func callServerStreaming(c hellopb.HelloServiceClient) {
	fmt.Println("Calling Unary callServerStreaming RPC call")
	request := &hellopb.HelloManyTimesRequest{
		Greet: &hellopb.Hello{
			FirstName: "Joey",
			LastName:  "Tribiyani",
		},
	}
	resStream, err := c.SayHelloManyTimes(context.Background(), request)
	if err != nil {
		log.Fatalf("error while calling SayHelloManyTimes() RPC: %v", err)
	}
	for {
		message, err := resStream.Recv()
		if err == io.EOF {
			// we reached the end of stream.
			break
		}
		if err != nil {
			log.Fatalf("error while reading the stream: %v", err)
		}
		log.Printf("Response from the SayHelloManyTimes: %v", message.GetMessage())

		log.Println("test after")
	}

}

func callServerStreamingArithmeticOperations(c hellopb.CalculatorServiceClient) {
	fmt.Println("Calling Unary PerformArithmeticOperations RPC call")
	request := &hellopb.PerformArithmeticOperationsRequest{
		Numbers: &hellopb.TwoNumbers{
			First:  10,
			Second: 5,
		},
	}
	resStream, err := c.PerformArithmeticOperations(context.Background(), request)
	if err != nil {
		log.Fatalf("error while calling PerformArithmeticOperations() RPC: %v", err)
	}
	for {
		message, err := resStream.Recv()
		if err == io.EOF {
			// we reached the end of stream.
			break
		}
		if err != nil {
			log.Fatalf("error while reading the stream: %v", err)
		}
		log.Printf("Response from the PerformArithmeticOperations: %v", message.GetMessage())
	}

}

func callClientSideStreamingRPC(c hellopb.HelloServiceClient) {
	fmt.Println("Calling Client side streaming SayLongHello() RPC call")
	requests := []*hellopb.LongHelloRequest{
		{
			Greet: &hellopb.Hello{
				FirstName: "Ross",
				LastName:  "Geller",
			},
		},
		{
			Greet: &hellopb.Hello{
				FirstName: "Monika",
				LastName:  "Geller",
			},
		},
		{
			Greet: &hellopb.Hello{
				FirstName: "Chandler",
				LastName:  "Bing",
			},
		},
		{
			Greet: &hellopb.Hello{
				FirstName: "Joey",
				LastName:  "Tribiyani",
			},
		},
		{
			Greet: &hellopb.Hello{
				FirstName: "Rachel",
				LastName:  "Green",
			},
		},
		{
			Greet: &hellopb.Hello{
				FirstName: "Phoebe",
				LastName:  "Buffay",
			},
		},
	}
	stream, err := c.SayLongHello(context.Background())
	if err != nil {
		log.Fatalf("Error while calling client side streaming RPC: %v", err)
	}
	// iterate the slice and send the data in the request.
	for _, req := range requests {
		fmt.Printf("Sending request for: %v\n", req)
		stream.Send(req)
		time.Sleep(2 * time.Second)
	}

	res, err := stream.CloseAndRecv()
	if err != nil {
		log.Fatalf("Error While reading the response: %v\n", err)
	}
	fmt.Printf("Response from LongHello Response: %v\n", res)
}

func callBiDirectionalStreamingRPC(c hellopb.HelloServiceClient) {
	fmt.Println("Calling Bi Directional streaming SayHelloEveryone() RPC call")
	requests := []*hellopb.HelloEveryoneRequest{
		{
			Greet: &hellopb.Hello{
				FirstName: "Ross",
				LastName:  "Geller",
			},
		},
		{
			Greet: &hellopb.Hello{
				FirstName: "Monika",
				LastName:  "Geller",
			},
		},
		{
			Greet: &hellopb.Hello{
				FirstName: "Chandler",
				LastName:  "Bing",
			},
		},
		{
			Greet: &hellopb.Hello{
				FirstName: "Joey",
				LastName:  "Tribiyani",
			},
		},
		{
			Greet: &hellopb.Hello{
				FirstName: "Rachel",
				LastName:  "Green",
			},
		},
		{
			Greet: &hellopb.Hello{
				FirstName: "Phoebe",
				LastName:  "Buffay",
			},
		},
	}

	stream, err := c.SayHelloEveryone(context.Background())
	if err != nil {
		log.Fatalf("Error while calling the Bi Directional streaming RPC API")
	}

	waitChan := make(chan struct{})
	// iterate over requests and send bunch of messages to the server (go-routine)
	go func() {
		// function to send messages to server
		for _, req := range requests {
			fmt.Printf("Sending a message: %v\n", req)
			stream.Send(req)
			time.Sleep(2 * time.Second)
		}
		stream.CloseSend()
	}()
	//receive bunch of messages from the server (go-routine)
	go func() {
		// function to receive messages from server
		for {
			res, err := stream.Recv()
			if err == io.EOF {
				break
			}
			if err != nil {
				log.Fatalf("Error while receiving: %v", err)
				break
			}
			fmt.Println("Received Response:", res.GetMessage())
		}
		close(waitChan)
	}()

	//block the main go-routines to finish above 2 go-routines
	<-waitChan
}

func callUnaryRPCWithDeadline(c hellopb.HelloServiceClient, timeout time.Duration) {
	fmt.Println("Calling Unary SayHelloWithDeadline RPC call")
	helloRequest := &hellopb.HelloWithDeadlineRequest{
		Greet: &hellopb.Hello{
			FirstName: "Chandler",
			LastName:  "Bing",
		},
	}
	ctx, cancel := context.WithTimeout(context.Background(), timeout)
	defer cancel()
	response, err := c.SayHelloWithDeadline(ctx, helloRequest)
	if err != nil {
		statusErr, ok := status.FromError(err)
		if ok {
			if statusErr.Code() == codes.DeadlineExceeded {
				fmt.Println("Timeout was hit! Deadline was exceeded")
			} else {
				fmt.Printf("Unexpected Error: %v\n", statusErr)
			}
		} else {
			log.Fatalf("Error while calling SayHelloWithDeadline RPC: %v", err)
		}
		return
	}
	log.Printf("Response from SayHelloWithDeadline: %v", response)
}
